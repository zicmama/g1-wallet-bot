# G1 Wallet Bot

## How it works

Telegram and Matrix bot that connects to a backend (in Django) using Jaklis + Duniter-GVA nodes to do quick operations (balance/historial/payments). 

Great to have a simple wallet for new people in the G1 economy.

## How to run
```
# Clone this repository via git
git clone git@git.duniter.org:kapis/g1-wallet-bot.git

# Download and install jaklis from https://git.p2p.legal/axiom-team/jaklis
cd g1-wallet-bot
git clone https://git.p2p.legal/axiom-team/jaklis.git
cd jaklis
bash setup.sh

# Add you  own settings
cd ..
cd g1superbotdjango
cp local_settings.template local_settings.py
nano local_settings.py  #edit settings adding your bot ID.

# Create virtualenv and install dependencies
# You need to have python3, django and virtual environment installed
cd ..
mkvirtualenv g1bot #create a python3 virtualenv
pip3 install -r requirements.txt
workon g1bot

# Run server
python manage.py runserver --settings=g1superbotdjango.local_settings

# if it's the first time apply migrations and create superuser
python3 manage.py showmigrations
python3 manage.py migrate
python3 manage.py createsuperuser

# Execute bot
workon g1bot
python manage.py launch_telegram_bot --settings=g1superbotdjango.local_settings
```

## Support
There are two chat groups to try both boths and report incidences, beside the git issues here, etc.

Telegram: https://t.me/+L488geJ9So45YzI0
Matrix/Element: #bot-g1:yuno.librezo.fr

## Contributions
You may contribute to this repo if you wish by forking it, setting the upstream repo and sending merge requests.


## Authors and acknowledgment
Main author: @kapis 

Dependencies: Thanks to @pokapow for the Jaklis software and all Duniter-GVA nodes' maintainers: @aya and @pini-gh.

## License
AGPLv3

## Project status
Under constant improvements.
