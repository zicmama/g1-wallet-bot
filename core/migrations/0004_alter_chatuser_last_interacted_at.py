# Generated by Django 4.2 on 2023-05-04 14:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_rename_last_interaction_chatuser_last_interacted_at_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='chatuser',
            name='last_interacted_at',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
