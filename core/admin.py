from django.contrib import admin
from .models import ChatUser
from django.utils import timezone
from datetime import datetime, timedelta

class Last30DaysFilter(admin.SimpleListFilter):
    title = 'Created within Last 30 Days'
    parameter_name = 'last_30_days'

    def lookups(self, request, model_admin):
        return (
            ('true', 'Yes'),
            ('false', 'No'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'true':
            today = timezone.now().date()
            last_30_days_start = today - timedelta(days=30)
            return queryset.filter(created_at__gte=last_30_days_start, created_at__lte=today)
        elif self.value() == 'false':
            return queryset.exclude(created_at__gte=timezone.now().date() - timedelta(days=30))
        return queryset


class UserAdmin(admin.ModelAdmin):
    list_display = ('chat_username', 'chat_first_name','chat_last_name', 'chat_id', 'lang', 'protocol', 'version', 'last_interacted_at', 'created_at')
    ordering = ('-last_interacted_at', 'chat_username', 'chat_id', 'lang', 'protocol', 'version', 'created_at')
    readonly_fields = ('last_interacted_at', 'created_at',)
    search_fields = ['chat_id', 'chat_username', 'chat_last_name', 'chat_first_name']
    list_filter = [
        ('created_at', admin.DateFieldListFilter),
        'lang',
        'protocol',
        Last30DaysFilter
    ]

    def changelist_view(self, request, extra_context=None):
        if not request.GET.get('created_at__gte') and not request.GET.get('created_at__lte'):
            # Set default filter to this month
            today = timezone.now().date()
            first_day_of_month = today.replace(day=1)
            last_day_of_month = (first_day_of_month + timedelta(days=32)).replace(day=1) - timedelta(days=1)
            params = request.GET.copy()
            params['created_at__gte'] = first_day_of_month
            params['created_at__lte'] = last_day_of_month
            request.GET = params
            request.META['QUERY_STRING'] = request.GET.urlencode()


        return super().changelist_view(request, extra_context)

admin.site.register(ChatUser, UserAdmin)