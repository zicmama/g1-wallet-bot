# -*- coding: utf-8 -*-
from django.db import models
from django.utils import timezone

class BotProtocolType(models.TextChoices):
    TELEGRAM = "TELEGRAM", "TELEGRAM"
    MATRIX = "MATRIX", "MATRIX"


class ChatUser(models.Model):
    chat_id = models.CharField(max_length=255, unique=True, null=False)
    chat_first_name = models.CharField(max_length=255, null=True, blank=True)
    chat_last_name = models.CharField(max_length=255, null=True, blank=True)
    chat_username = models.CharField(max_length=255, null=True, blank=True, db_index=True)
    pubkey = models.CharField(max_length=45, null=True, db_index=True)
    lang = models.CharField(max_length=10, null=True)
    protocol = models.CharField(
        max_length=15,
        choices=BotProtocolType.choices,
        default=BotProtocolType.TELEGRAM,
        null=False,
        blank=False
    )
    version = models.FloatField(default=1.0, null=False)
    created_at = models.DateTimeField(default=timezone.now, null=False)
    last_interacted_at = models.DateTimeField(null=True, blank=True)

 

    def __str__(self):
        name = str(self.chat_id)
        if self.chat_first_name:
            name += f" ({self.chat_first_name} {self.chat_last_name})"
        if self.chat_username:
            name += f" @{self.chat_username}"
        return name