#!/usr/bin/env python
# pylint: disable=unused-argument, wrong-import-position
# This program is dedicated to the public domain under the CC0 license.

import argparse, sys, os, getpass, string, random
from os.path import join, dirname
from shutil import copyfile
from core.bot_telegram import scheduled
#from dotenv import load_dotenv
import logging
import sys
from telegram import ReplyKeyboardMarkup, __version__ as TG_VER
from telegram.constants import ParseMode
from django.conf import settings
from asgiref.sync import sync_to_async
import asyncio


import html
import json

import traceback

from .utils import _
from core.models import ChatUser
from . import utils
from . import TYPING_AMOUNT, WAITING_MAIN_BUTTON, TYPING_PUBKEY, TYPING_CESIUM_NAME, CHOOSING, TYPING_AMOUNT, TYPING_DESCRIPTION, TYPING_SEARCHTERM, CHOOSING_FROM_SEARCH, __version__, MINIMUM_AMOUNT_TX, logger

from core import monitoring
# Autocreating .env file if does not exist

# dir_path = os.path.dirname(os.path.realpath(__file__))
# dotenv_path = join(dir_path, ".env")
# if not os.path.isfile(dotenv_path):
#     envtemplate = join(dir_path, "env.template")
#     copyfile(envtemplate, dotenv_path)
#     print('Private configuration file ".env" generated. You should edit. ')

# # Loading .env vars
# load_dotenv(dotenv_path)


from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update, ForceReply
from telegram.ext import (
    Application,
    CallbackQueryHandler,
    CommandHandler,
    CallbackContext,
    ContextTypes,
    ConversationHandler,
    MessageHandler,
    filters,
    JobQueue,
    Job
)



def get_main_buttons(updt, ctx):
    chat_id = updt.effective_chat.id
    MAIN_BUTTONS = [
        _(chat_id, ctx, "PAY_BTN"),
        _(chat_id, ctx, "ACCOUNT_BTN"),
        _(chat_id, ctx, "SEARCH_BTN"),
        _(chat_id, ctx, "SETTINGS_BTN"),
    ]
    return MAIN_BUTTONS


async def show_type_amount(update, context):
    chat_id = update.effective_chat.id
    keyboard = [
        [
            InlineKeyboardButton(_(chat_id, context, "CANCEL_PAYMENT"), callback_data="cancel"),
        ]
    ]

    reply_markup = InlineKeyboardMarkup(keyboard)
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        reply_markup=reply_markup,
        text=_(chat_id, context, "G1_AMOUNT_QUESTION"),
    )
    return TYPING_AMOUNT


async def read_amount(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    print("Waiting to Read amount")
    
    query = update.callback_query
    if query:
        await query.answer()
        await query.edit_message_reply_markup(reply_markup=None)
        if query.data == "cancel":
            return await show_main_buttons(update, context)
    else:
        answer = update.message.text
        chat_user = update.message.from_user
        chat_id = update.effective_chat.id
        lang = await utils.set_user_ctx_lang(chat_user, chat_id, context)
        if not lang:
            return await utils.show_languages(chat_id, context)

        if answer in get_main_buttons(update, context):
            return await show_main_buttons(update, context)

        amount = update.message.text.replace(",", ".")
        # Check if amount is a valid float number
        try:
            f_amount = float(amount)
        except:
            
            await update.message.reply_text(
                text=_(chat_id, context, "ERROR_AMOUNT_FORMAT").format(minimum=MINIMUM_AMOUNT_TX),
                parse_mode=ParseMode.HTML,
            )
            return await show_type_amount(update, context)

        if f_amount < MINIMUM_AMOUNT_TX:
            await update.message.reply_text(
                text=_(chat_id, context, "TXT_ERROR_MIN_TX_AMOUNT").format(minimum=MINIMUM_AMOUNT_TX),
                parse_mode=ParseMode.HTML,
            )
            return await show_type_amount(update, context)
        context.user_data["amount"] = amount
        keyboard = [
            [
                InlineKeyboardButton(_(chat_id, context, "BTN_NO_COMMENT"), callback_data="nocomment"),
                InlineKeyboardButton(_(chat_id, context, "BTN_CANCEL_PAYMENT"), callback_data="cancel"),
            ]
        ]

        reply_markup = InlineKeyboardMarkup(keyboard)
        await context.bot.send_message(
            chat_id=chat_id,
            reply_markup=reply_markup,
            text=_(chat_id, context, "COMMENT_QUESTION"),
        )
        return TYPING_DESCRIPTION

async def tx_monitoring(context: CallbackContext):
    pubkey = context.job.data.get('pubkey')
    chat_id = context.job.data.get('chat_id')
    message_id = context.job.data.get('message_id')
    print('tx_monitoring')
    ok = await utils.check_new_tx(pubkey, context)
    if ok:
        msg = "Transaction confirmed"
    else:
        msg = "Transaction failed"
    
    await context.bot.send_message(
        chat_id=chat_id,
        text=msg,
        reply_to_message_id=message_id
    )

async def read_payment_comment(
    update: Update, context: CallbackContext
) -> int:
    print("Read Payment comment")
    query = update.callback_query
    if query:
        await query.answer()
        await query.edit_message_reply_markup(reply_markup=None)
        if query.data == "cancel":
            return await show_main_buttons(update, context)
        elif query.data == "nocomment":
            comment = ""
    else:
        answer = update.message.text
        if answer in get_main_buttons(update, context):
            return await main_button_pushed(update, context, answer)
        comment = answer

    pubkey = context.user_data["pubkey"]
    amount = context.user_data["amount"]

    chat_id = update.effective_chat.id
    response_tx = ""
    mypubkey, dunikey = utils.get_user_dunikey(str(chat_id))
    if mypubkey == pubkey:
        text = _(chat_id, context, "ERROR_SAME_BOT_PUBKEY")
    else:
        comment = utils.clean_comment(comment)
        response_tx = utils.payment(dunikey, pubkey, amount, comment, pool=False)
        print("response", response_tx)
        comment_txt = ""
        if comment:
            comment_txt = _(chat_id, context, "WITH_COMMENT").format(comment=comment)

        successful_msg = _(chat_id, context, "PAYMENT_SENT").format(
            amount=amount, comment_txt=comment_txt, pubkey=pubkey
        )
        if response_tx == utils.TX_SUCCESS:
            text = successful_msg
        elif response_tx == utils.TX_NO_BALANCE:
            response_tx = utils.payment(dunikey, pubkey, amount, comment, pool=True)
            if response_tx == utils.TX_SUCCESS:
                text = successful_msg
            elif response_tx == utils.TX_NO_BALANCE:
                text = _(chat_id, context, "ERROR_NOT_ENOUGH_BALANCE")
        else:
            text = _(chat_id, context, "ERROR_UNKNOWN")

    message = await context.bot.send_message(
        chat_id=chat_id, text=text, parse_mode=ParseMode.HTML
    )
    if response_tx == utils.TX_SUCCESS:
        #Monitoring if tx gets lost or confirmed.
        print('Launching task')
        job_queue = context.job_queue
        job_context = {'chat_id': chat_id, 'pubkey': pubkey, 'message_id': message.message_id}

        #job_queue.run_once(tx_monitoring, when=2, data=job_context)



    return await show_main_buttons(update, context)


async def main_button_pushed(update, context, text):
    chat_id = update.effective_chat.id
    chat_user = update.message.from_user
    lang = await utils.set_user_ctx_lang(chat_user, chat_id, context)
    if not lang:
        return await utils.show_languages(chat_id, context)
    [PAY_BTN, ACCOUNT_BTN, SEARCH_BTN, SETTINGS_BTN] = get_main_buttons(update, context)

    if text == PAY_BTN:
        print("Payment button pushed")
        context.user_data["step"] = "pubkey_to_pay"
        keyboard = [
            #[InlineKeyboardButton("QR", callback_data="search_qr")],
            [InlineKeyboardButton(_(chat_id, context, "CANCEL"), callback_data="cancel")]]

        reply_markup = InlineKeyboardMarkup(keyboard)
        await context.bot.send_message(
            chat_id=chat_id,
            reply_markup=reply_markup,
            text=_(chat_id, context, "ASKING_PUBKEY_DESTINATION"),
        )

        return TYPING_PUBKEY
    elif text == ACCOUNT_BTN:
        context.user_data["step"] = "receive"
        keyboard = [
            [
                InlineKeyboardButton(_(chat_id, context, "MY_PUBKEY"), callback_data="mypublickey"),
                InlineKeyboardButton(_(chat_id, context, "MY_LAST_TRANSACTIONS"), callback_data="myhistorial"),
            ],
            
            [InlineKeyboardButton(_(chat_id, context, "MY_BALANCE"), callback_data="mybalance"),
             InlineKeyboardButton(_(chat_id, context, "CHANGE_NAME_BUTTON"), callback_data="changemyname")
            ]

        ]

        reply_markup = InlineKeyboardMarkup(keyboard)
        await update.message.reply_text(_(chat_id, context, "CHOOSE_OPTION"), reply_markup=reply_markup)
        return CHOOSING
    elif text == SEARCH_BTN:
        context.user_data["step"] = "pubkey_to_search"
        keyboard = [#[InlineKeyboardButton("QR", callback_data="search_qr")],
                    [InlineKeyboardButton(_(chat_id, context, "CANCEL"), callback_data="cancel")]]

        reply_markup = InlineKeyboardMarkup(keyboard)
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            reply_markup=reply_markup,
            text=_(chat_id, context, "WRITE_SEARCHTERM_OR_PUBKEY"),
        )
        return TYPING_SEARCHTERM


    elif text == SETTINGS_BTN:
        keyboard = [
            [
                InlineKeyboardButton(_(chat_id, context, "CHANGE_LANG"), callback_data="change_language"),
                InlineKeyboardButton(_(chat_id, context, "HELP"), callback_data="support"),
            ],
        ]

        reply_markup = InlineKeyboardMarkup(keyboard)
        await update.message.reply_text(_(chat_id, context, "CHOOSE_OPTION"), reply_markup=reply_markup)
        return CHOOSING


    else:
        print("Other main button pushed")
        return await show_main_buttons(update, context)

async def read_cesium_name(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    query = update.callback_query
    if query:
        await query.answer()
        await query.edit_message_reply_markup(reply_markup=None)
        if query.data == "cancel":
            return await show_main_buttons(update, context)
    else:
        answer = update.message.text
        chat_id = update.effective_chat.id
        chat_user = update.message.from_user
        ok = await utils.set_user_cesium_name(chat_user, chat_id, context, answer)
        if ok:
            text = _(chat_id, context, "CHANGE_NAME_SUCCESS")
        else:
            text = _(chat_id, context, "ERROR_UNKNOWN")
        await context.bot.send_message(
            chat_id,
            text,
            parse_mode=ParseMode.HTML,
        )
        return await main_button_pushed(update, context, text)

async def read_pubkey(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    query = update.callback_query
    chat_id = update.effective_chat.id
    user_session = context.user_data.setdefault(chat_id, {})

    # Check if it's the user's first interaction
    if not user_session.get('started'):
        # Perform any necessary actions for the first interaction
        user_session['started'] = True
        # print(f"User with ID {chat_id} started interacting with the bot for the first time.")
        await utils.update_last_interacted_at(chat_id)

    # Handle the user interaction and perform any necessary actions
    # print(f"User with ID {chat_id} is continuing the interaction.")
    if query:
        await query.answer()
        await query.edit_message_reply_markup(reply_markup=None)
        if query.data == "cancel":
            return await show_main_buttons(update, context)
        elif query.data == "search_qr":
            msg = _(chat_id, context, "QR_INFO_1")
            await context.bot.send_message(
                chat_id=chat_id,
                text=msg,
                parse_mode=ParseMode.HTML,
            )
            return await show_main_buttons(update, context)
    else:
        user = update.message.from_user        
        chat_user = update.message.from_user
        lang = await utils.set_user_ctx_lang(chat_user, chat_id, context)
        if not lang:
            return await utils.show_languages(chat_id, context)

        is_photo = update.message.photo
        if is_photo:
            image = update.message.photo[-1].file_id
            obj = await context.bot.get_file(image)
            pubkey, ok = utils.check_online_qr_pubkey(obj.file_path)
    
            if ok:            
                if "step" in context.user_data and context.user_data["step"] == "pubkey_to_pay":
                #Only show the pubkey detected message if clicking on the pay button. 
                    await context.bot.send_message(
                        chat_id=chat_id,
                        text=f'Public key found:\n<code>{pubkey}</code>',
                        parse_mode=ParseMode.HTML,
                    )
            else:
                await context.bot.send_message(
                    chat_id=chat_id,
                    text='Error. No QR or public key found. Retry again with better photo.',
                    parse_mode=ParseMode.HTML,
                )
        else:
            text = update.message.text
            if text in get_main_buttons(update, context):
                return await main_button_pushed(update, context, text)
            print(f'Read Pubkey: Msg received by: {user.first_name}: "{text}"')
            pubkey, ok = utils.check_pubkey(text)

        if not ok:
            context.user_data.pop("pubkey", None)
            if "step" in context.user_data and context.user_data["step"] == "pubkey_to_search":
                return await utils.search_term(context, chat_id, text)
            else:
                keyboard = [[InlineKeyboardButton(_(chat_id, context, "CANCEL"), callback_data="cancel")]]

                reply_markup = InlineKeyboardMarkup(keyboard)
                await context.bot.send_message(
                    chat_id=update.effective_chat.id,
                    reply_markup=reply_markup,
                    text=_(chat_id, context, "WRITE_VALID_PUBKEY"),
                )
                return TYPING_PUBKEY

        context.user_data["pubkey"] = pubkey

    if "step" in context.user_data and context.user_data["step"] == "pubkey_to_pay":
        return await show_type_amount(update, context)

    keyboard = [
        [
            InlineKeyboardButton(_(chat_id, context, "SEND_MONEY"), callback_data="pubkey_pay"),
            InlineKeyboardButton(_(chat_id, context, "TRANSACTIONS"), callback_data="pubkey_historial"),
        ],
        [
            InlineKeyboardButton(_(chat_id, context, "BALANCE"), callback_data="pubkey_balance"),
            InlineKeyboardButton(_(chat_id, context, "PROFILE_DETAILS"), callback_data="pubkey_profile"),
        ],
        [
            InlineKeyboardButton(_(chat_id, context, "CANCEL"), callback_data="search_cancel"),
        ],
    ]

    reply_markup = InlineKeyboardMarkup(keyboard)
    await update.message.reply_text(
        _(chat_id, context, "CHOOSE_OPTION_FOR_PUBKEY").format(pubkey=pubkey),
        reply_markup=reply_markup,
        parse_mode=ParseMode.HTML,
    )
    return CHOOSING

async def change_name(update, context, chat_id):
    keyboard = [[InlineKeyboardButton(_(chat_id, context, "CANCEL"), callback_data="cancel")]]
    reply_markup = InlineKeyboardMarkup(keyboard)
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        reply_markup=reply_markup,
        text=_(chat_id, context, "CHANGE_NAME"),
    )

    return TYPING_CESIUM_NAME


async def searchitem_chosen(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Parses the CallbackQuery and updates the message text."""
    query = update.callback_query
    await query.answer()
    chat_id = query.message.chat.id
    username = query.message.chat.username
    chat_user = query.message.from_user
    print(f'Query button pushed by "{username}" (#{chat_id}): ', query.data)

    if query.data == "search_cancel":
        return await show_main_buttons(update, context)
    else:
        pubkey = query.data
        context.user_data["pubkey"] = pubkey

    keyboard = [
        [
            InlineKeyboardButton(_(chat_id, context, "SEND_MONEY"), callback_data="pubkey_pay"),
            InlineKeyboardButton(_(chat_id, context, "TRANSACTIONS"), callback_data="pubkey_historial"),
        ],
        [
            InlineKeyboardButton(_(chat_id, context, "BALANCE"), callback_data="pubkey_balance"),
            InlineKeyboardButton(_(chat_id, context, "PROFILE_DETAILS"), callback_data="pubkey_profile"),
        ],
        [
            InlineKeyboardButton(_(chat_id, context, "CANCEL"), callback_data="search_cancel"),
        ],
    ]

    reply_markup = InlineKeyboardMarkup(keyboard)
    query.edit_message_reply_markup

    await context.bot.send_message(
        text=_(chat_id, context, "CHOOSE_OPTION_FOR_PUBKEY").format(pubkey=pubkey),
        reply_markup=reply_markup,
        parse_mode=ParseMode.HTML,
        chat_id=chat_id,
    )
    return CHOOSING
    


async def action_chosen(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Parses the CallbackQuery and updates the message text."""
    query = update.callback_query
    await query.answer()
    chat_id = query.message.chat.id
    username = query.message.chat.username
    chat_user = query.message.from_user

    print(f'Action chosen: Button pushed by "{username}" (#{chat_id}): ', query.data)
    await query.edit_message_reply_markup(reply_markup=None)
    # Check if it's the user's first interaction
    user_session = context.user_data.setdefault(chat_id, {})
    
    # Check if it's the user's first interaction
    if not user_session.get('started'):
        # Perform any necessary actions for the first interaction
        user_session['started'] = True
        # print(f"User with ID {chat_id} started interacting with the bot for the first time.")
        await utils.update_last_interacted_at(chat_id)

    # Handle the user interaction and perform any necessary actions
    # print(f"User with ID {chat_id} is continuing the interaction.")

    if query.data in utils.LANGUAGES:
        lang = query.data
        print('Language chosen', lang)
        await utils.set_language(chat_id, chat_user, context, lang)
        if "start" in context.user_data and context.user_data["start"] == True:
            pubkey, dunikey = utils.get_user_dunikey(str(chat_id))
            context.user_data["start"] = False
            await utils.show_mypublickey_qr(context, chat_id, pubkey)
    else:
        lang = await utils.set_user_ctx_lang(chat_user, chat_id, context)
        if not lang:
            return await utils.show_languages(chat_id, context)
        
        elif query.data == "search_cancel":
            pass

        elif query.data in ["pubkey_profile", "pubkey_historial", "pubkey_pay", "pubkey_balance"]:
            if "pubkey" in context.user_data:
                pubkey = context.user_data["pubkey"]
                if query.data == "pubkey_profile":
                    await utils.pubkey_details(context, chat_id, pubkey)
                elif query.data == "pubkey_historial":
                    await utils.history_details(context, chat_id, pubkey)
                elif query.data == "pubkey_pay":
                    return await show_type_amount(update, context)
                elif query.data == "pubkey_balance":
                    await utils.show_balance(context, chat_id, pubkey)
            else:
                msg = "Connection lost. Repeat public key." #_(chat_id, context, "BOT_INFO_1")
                await context.bot.send_message(
                    chat_id=chat_id,
                    text=msg,
                    parse_mode=ParseMode.HTML,
                )

        elif query.data == "mypublickey":
            pubkey, dunikey = utils.get_user_dunikey(str(chat_id))
            await utils.show_mypublickey_qr(context, chat_id, pubkey)

        elif query.data == "myhistorial":
            pubkey, dunikey = utils.get_user_dunikey(str(chat_id))
            await utils.history_details(context, chat_id, pubkey)

        elif query.data == "mybalance":
            pubkey, dunikey = utils.get_user_dunikey(str(chat_id))
            await utils.show_balance(context, chat_id, pubkey)

        elif query.data == "changemyname":
            pubkey, dunikey = utils.get_user_dunikey(str(chat_id))
            return await change_name(update, context, chat_id)


        elif query.data == "change_language":
            return await utils.show_languages(chat_id, context)

        elif query.data == "support":
            msg = _(chat_id, context, "BOT_INFO_1").format(version=__version__)
            msg += "\n\n"+ _(chat_id, context, "BOT_INFO_2").format(key="<code>24jaf8XhYZyDyUb7hMcy5qsanaHBC11AwPefcCQRBQNA</code>")
            msg += "\n\n" + _(chat_id, context, "BOT_INFO_3").format(url="https://t.me/+L488geJ9So45YzI0")
            await context.bot.send_message(
                chat_id=chat_id,
                text=msg,
                parse_mode=ParseMode.HTML,
            )

    await show_main_buttons(update, context)


async def show_main_buttons(update, context):
    context.user_data.pop("pubkey", None)
    context.user_data.pop("step", None)
    reply_keyboard = [get_main_buttons(update, context)]
    # await update.message.reply_text(
    chat_id = update.effective_chat.id
    text = _(chat_id, context, "BUTTON_OR_PUBKEY")
    await context.bot.send_message(
        chat_id=chat_id,
        text=text,
        parse_mode=ParseMode.HTML,
        reply_markup=ReplyKeyboardMarkup(
            reply_keyboard,
            one_time_keyboard=False,
            resize_keyboard=True,
            input_field_placeholder=text,
        ),
    )

    return WAITING_MAIN_BUTTON

async def stop(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    # Stop the bot
    update.stop()


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Sends a message with three inline buttons attached."""
    chat_user = update.message.from_user
    chat_id = update.effective_chat.id
    
    if chat_id > 0:  # ChatUser chat and not group
        #db_user = await get_user_or_create_db(chat_user, chat_id)
        #Disabled getting lang from database, we always ask if command /start
        # lang = await utils.set_user_ctx_lang(chat_user, chat_id, context)
        # if not lang:
        context.user_data["start"] = True
        return await utils.show_languages(chat_id, context)


        
        # await context.bot.send_message(chat_id=update.effective_chat.id, text="Pega aquí una <b>llave pública G1</b> para hacer una transacción a esa llave. Puedes usar la app lectora de QRs de tu móvil, o instalar <a href='https://github.com/wewewe718/QrAndBarcodeScanner/'>QrAndBarcodeScanner</a> que es libre. También puedes usar @qrcrbot enviandole una foto del QR a pagar.", parse_mode=ParseMode.HTML, disable_web_page_preview=True)

        return await show_main_buttons(update, context)


async def error_handler(update: object, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Log the error and send a telegram message to notify the developer."""
    # Log the error before we do anything else, so we can see it even if something breaks.
    logger.error(msg="Exception while handling an update:", exc_info=context.error)

    # traceback.format_exception returns the usual python message about an exception, but as a
    # list of strings rather than a single string, so we have to join them together.
    tb_list = traceback.format_exception(
        None, context.error, context.error.__traceback__
    )
    tb_string = "".join(tb_list)

    # Build the message with some markup and additional information about what happened.
    # You might need to add some logic to deal with messages longer than the 4096 character limit.
    update_str = update.to_dict() if isinstance(update, Update) else str(update)
    message = (
        f"An exception was raised while handling an update\n"
        f"<pre>update = {html.escape(json.dumps(update_str, indent=2, ensure_ascii=False))}"
        "</pre>\n\n"
        f"<pre>context.chat_data = {html.escape(str(context.chat_data))}</pre>\n\n"
        f"<pre>context.user_data = {html.escape(str(context.user_data))}</pre>\n\n"
        f"<pre>{html.escape(tb_string)}</pre>"
    )

    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="Se ha producido un error en el bot. Inténtalo más tarde o pulsa el botón de ayuda.",
    )
    await context.bot.send_message(
        chat_id=settings.TELEGRAM_DEVELOPER_CHAT_ID,
        text=message,
        parse_mode=ParseMode.HTML,
    )
    context.user_data.pop("pubkey", None)
    context.user_data.pop("step", None)
    return ConversationHandler.END

