import logging

# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__name__)


__version__ = "0.5.2"
MINIMUM_AMOUNT_TX = 1
(
    WAITING_MAIN_BUTTON,
    TYPING_PUBKEY,
    CHOOSING,
    TYPING_AMOUNT,
    TYPING_DESCRIPTION,
    TYPING_CESIUM_NAME,
    TYPING_SEARCHTERM,
    CHOOSING_FROM_SEARCH
) = range(8)
