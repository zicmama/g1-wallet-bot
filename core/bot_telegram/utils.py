import os
import json
import re
import datetime
from os import listdir
from os.path import isfile, join
from duniterpy.key import SigningKey
import subprocess
import traceback
import unidecode
import qrcode
from telegram.constants import ParseMode
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update, ForceReply
from telegram.ext import ContextTypes, CallbackContext
from django.conf import settings
from asgiref.sync import sync_to_async
import requests

import cv2
from pyzbar.pyzbar import decode

from collections import defaultdict
import time
from pprint import pprint


#from core.models import *
from core import bma
from core.models import ChatUser

from . import TYPING_AMOUNT, WAITING_MAIN_BUTTON, TYPING_PUBKEY, CHOOSING, TYPING_AMOUNT, TYPING_DESCRIPTION, CHOOSING_FROM_SEARCH, __version__

TX_SUCCESS, TX_NO_BALANCE, TX_ERROR = range(3)
PUBKEY_REGEX = "(?![OIl])[1-9A-Za-z]{42,45}(:...)*?"

LANGUAGES = ["es", "ca", "de", "en", "eo", "eu", "fr", "gl", "it", "pt"] 
LANGUAGES_NAMES = ["Castellano", "Català", "Deutsch", "English", "Esperanto", "Euskera", "Français", "Galego", "Italiano", "Português"]

if 'translations' not in globals():
    translations = {}
    for lang_code in LANGUAGES:
        print('Loading translation for ', lang_code)
        with open(f"i18n/messages_{lang_code}.json") as f:
            translations[lang_code] = json.load(f)

async def get_user_or_create_db(chat_user, chat_id):
    db_user = await get_user_db(chat_id)
    if not db_user:
        print("Creating user in db")
        pubkey, __ = get_user_dunikey(str(chat_id))
        db_user = await new_db_user(
            chat_id, chat_user.username, chat_user.first_name, chat_user.last_name, pubkey
        )
    else:
        print("Found user in db: ", db_user)
        print("With pubkey", db_user.pubkey)
    return db_user


async def get_user_db(chat_id):
    return await sync_to_async(ChatUser.objects.filter(chat_id=chat_id).first)()

async def new_db_user(chat_id, username, firstname, lastname, pubkey):
    # Insert a new user
    user = await sync_to_async(ChatUser)(
        chat_id=chat_id,
        chat_first_name=firstname,
        chat_last_name=lastname,
        chat_username=username,
        pubkey=pubkey,
    )
    await sync_to_async(user.save)()
    return user

async def set_user_ctx_lang(chat_user, chat_id, ctx):
    if "lang" in ctx.user_data:
        lang = ctx.user_data["lang"]
    else:
        print('Checking in database...')
        db_user = await get_user_or_create_db(chat_user, chat_id)
        if db_user and db_user.lang:
            lang = db_user.lang
            ctx.user_data["lang"] = lang
        else:
            return False
    return True


def get_user_translation(chat_id, ctx, key):
    if "lang" in ctx.user_data:
        lang = ctx.user_data["lang"]
        if lang in translations:
            return translations[lang].get(key, key)
        else:
            return key
    else:
        print('Error: user not getting his ctx.user_data[lang] yet defined.')




_ = lambda chat_id, ctx, key: get_user_translation(chat_id, ctx, key)


async def show_languages(chat_id, context: ContextTypes.DEFAULT_TYPE) -> int:
    keyboard = []
    for i, lang in enumerate(LANGUAGES_NAMES):
        keyboard.append([InlineKeyboardButton(lang, callback_data=LANGUAGES[i])])

    reply_markup = InlineKeyboardMarkup(keyboard)
    await context.bot.send_message(
        chat_id=chat_id,
        reply_markup=reply_markup,
        text="Choose language | Elige idioma | Choisissez une langue | Elektu lingvon:",
    )
    
    return CHOOSING

async def set_language(chat_id, chat_user, context, lang):
    context.user_data["lang"] = lang
    
    db_user = await get_user_or_create_db(chat_user, chat_id)
    db_user.lang = lang
    await sync_to_async(db_user.save)()
    print(f"Language saved for user {db_user} to {lang}")

    await context.bot.send_message(
        chat_id=chat_id,
        text=_(chat_id, context, "LANGUAGE")+f": <b>{db_user.lang}</b>",
        parse_mode=ParseMode.HTML
    )

#TODO: apply only when user initiates or finalizes the telegram session
async def update_last_interacted_at(chat_id):
    print('Saving last interaction for user ', chat_id)
    db_user = await get_user_db(chat_id)
    if db_user:
        db_user.last_interacted_at = datetime.datetime.utcnow()
        await sync_to_async(db_user.save)()

def check_pubkey(msg):
    if msg:
        pubkey = ""
        if not re.match(PUBKEY_REGEX, msg):
            ok = False
        else:
            if ":" in msg:
                pubkey = msg[: msg.find(":")]
            else:
                pubkey = msg
            ok = True

        return pubkey, ok
    else: return _, False


def detect_qr_code(image_path):
    img = cv2.imread(image_path)
    decoded_objects = decode(img)
    for obj in decoded_objects:
        print('Data:', obj.data.decode('utf-8'))
        return obj.data.decode('utf-8')
    return False

def search_term_in_cesium_plus(term):
    url = f"{settings.CESIUMPLUS_NODE}/user,page,group/profile,record/_search"
    
    query = {
        "bool": {
            "should": [
                { "match": { "title": { "query": term, "boost": 2 } } }
            ]
        }
    }

    highlight = {
        "fields": {
            "title": {},
            "tags": {}
        }
    }

    from_value = 0
    size_value = 40

    _source = [
        "title",
        "city",
        "creationTime",
        "membersCount",
        "type",
        "wot"
    ]

    indices_boost = {
        "user": 100,
        "page": 1,
        "group": 0.01
    }

    payload = {
        "query": query,
        "highlight": highlight,
        "from": from_value,
        "size": size_value,
        "_source": _source,
        "indices_boost": indices_boost
    }

    response = requests.post(url, json=payload)

    # Handle the response as needed
    if response.status_code == 200:
        data = response.json()
        pprint(data)
        results = []
        hits = data.get("hits", {}).get("hits", [])
        
        for hit in hits:
            entry = {
                "title": hit.get("_source", {}).get("title", ""),
                "pubkey": hit.get("_id", ""),
                "city": hit.get("_source", {}).get("city", ""),
            }
            results.append(entry)
        return results, True
        # Process the data returned in the response
    else:
        print(f"Request failed with status code {response.status_code}")
        return _, False


async def search_term(context, chat_id, term):
    print(f'Searching term: "{term}"')
    results, ok = search_term_in_cesium_plus(term)
    keyboard = []
    if not ok:
        text_result = "Error connecting with server"
    else:
        text_result = f"{len(results)} results found."

    for r in results:
        city = f"({r['city']})" if r["city"] else ''
        button_term = InlineKeyboardButton(f'{r["title"]} {city} : {r["pubkey"][:10]}', callback_data=r["pubkey"])
        keyboard.append([button_term])

    keyboard.append([InlineKeyboardButton(_(chat_id, context, "CANCEL"), callback_data="cancel")])

    reply_markup = InlineKeyboardMarkup(keyboard)
    await context.bot.send_message(
        chat_id=chat_id,
        reply_markup=reply_markup,
        text=text_result#_(chat_id, context, "WRITE_VALID_PUBKEY"),
    )
    return CHOOSING_FROM_SEARCH

def check_online_qr_pubkey(url):
    path_parts = url.split('/')
    # Get the last part of the path, which should be the filename
    filename = path_parts[-1]
    try:
        response = requests.get(url, stream=True)
        response.raise_for_status()
        tmp_filename_image = join(settings.TEMP_PATH, filename)
        with open(tmp_filename_image, 'wb') as file:
            for chunk in response.iter_content(chunk_size=8192):
                file.write(chunk)
        pubkey = detect_qr_code(tmp_filename_image)
        pubkey, ok = check_pubkey(pubkey)
        if ok:     
            return pubkey, True  # Indicate that the download was successful
        else: return _, False

    except requests.exceptions.RequestException as e:
        print("Error while downloading image:", e)
        return _, False  # Indicate that the download failed

def get_tx_history(pubkey):
    command = f"jaklis -n '{settings.GVA_NODE}' history -p {pubkey} -j"
    print("command:", command)
    response_json = subprocess.getoutput(command)

    try:
        response = json.loads(response_json)
        # print('responseJSON', response)
    except Exception as error:
        print(traceback.format_exc())
        # print('Caught this error: ' + repr(error))
        response = {"error": True}

    return response


def get_qr(pubkey):
    # Data to be encoded
    qr_file = join(settings.QR_PATH, pubkey + ".png")
    if not os.path.isfile(qr_file):
        img = qrcode.make(pubkey)
        img.save(qr_file)
    return qr_file


async def show_mypublickey_qr(context, chat_id, pubkey):
    qr_file = get_qr(pubkey)
    await context.bot.send_photo(chat_id=chat_id, photo=qr_file)
    intro_txt = _(chat_id, context, "ACCOUNT_PUBKEY")
    await context.bot.send_message(
        chat_id=chat_id, text=intro_txt, parse_mode=ParseMode.HTML
    )
    await context.bot.send_message(
        chat_id=chat_id, text="<code>{pubkey}</code>".format(pubkey=pubkey), parse_mode=ParseMode.HTML
    )
    


async def show_historial(context, chat_id, pubkey):
    await history_details(context, chat_id, pubkey)


async def show_balance(context, chat_id, pubkey):
    await context.bot.send_message(
        chat_id, text=_(chat_id, context, "CHECKING_BALANCE"), parse_mode=ParseMode.HTML
    )
    await get_bma_balance(context, chat_id, pubkey)


async def pubkey_details(context, chat_id, pubkey):

    await get_gva_id(context, chat_id, pubkey)
    await profile_details(context, chat_id, pubkey)


async def get_gva_id(context, chat_id, pubkey):
    print("Checking GVA Id for Pubkey", pubkey)
    command = f"jaklis -n '{settings.GVA_NODE}' id -p {pubkey}"
    response_json = run_json_command(command)
    if not "error" in response_json:
        msg = ""
        isMember = False
        if response_json["idty"] is not None:
            isMember = response_json["idty"]["isMember"]
            username = response_json["idty"]["username"]
            msg += _(chat_id, context, "PSEUDO_USERNAME").format(username=username)
        ismember_bool_txt = (
            _(chat_id, context, "YES") if isMember else _(chat_id, context, "NO")
        )
        msg += "\n" + _(chat_id, context, "IS_MEMBER").format(ismember_bool_txt=ismember_bool_txt)
        await context.bot.send_message(chat_id, text=msg, parse_mode=ParseMode.HTML)



def remove_terminal_injections(input_string):
    # List of characters to remove
    chars_to_remove = ['`', '|', ';', '&', '$', '(', ')', '<', '>', '*', '?', '[', ']', '{', '}', "'", '"', '\\']

    # Remove the characters from the string
    sanitized_string = re.sub(r'[' + re.escape(''.join(chars_to_remove)) + ']', '', input_string)

    return sanitized_string

async def set_user_cesium_name(chat_user, chat_id, context, answer):
    _, dunikey = get_user_dunikey(str(chat_id))
    superbot_name = remove_terminal_injections(answer)
    superbot_name = f"{answer} 🤖" 
    command = f"jaklis -k {dunikey} -n '{settings.CESIUMPLUS_NODE}' set --name '{superbot_name}'"
    print("command:", command)
    response_json = subprocess.getoutput(command)
    print("response_json:", response_json)
    if "None" in response_json:
        return False
    elif check_pubkey(response_json)[1] == True:
        return True
    else:
        return False

def cesium_details(pubkey):
    print("Checking Cesium profile for Pubkey", pubkey)
    command = f"jaklis -n {settings.CESIUMPLUS_NODE} get -p {pubkey}"
    print("command:", command)
    response_json = subprocess.getoutput(command)
    if "Profile vide" in response_json:
        return ''
    else:
        response_json = json.loads(response_json)
        if not "error" in response_json and "title" in response_json:
            title = response_json["title"]
            return title
        else:
            return False

async def profile_details(context, chat_id, pubkey):
    title = cesium_details(pubkey)
    if title == '':
        msg = _(chat_id, context, "PROFILE_EMPTY")
        await context.bot.send_message(chat_id, text=msg, parse_mode=ParseMode.HTML)
    elif title == False:
        msg = msg = _(chat_id, context, "ERROR_GETTING_PROFILE")
        await context.bot.send_message(chat_id, text=msg, parse_mode=ParseMode.HTML)
    else:
        msg = _(chat_id, context, "PROFILE_NAME").format(title=title)
        await context.bot.send_message(chat_id, text=msg, parse_mode=ParseMode.HTML)


async def history_details(context, chat_id, pubkey):
    tx_history = get_tx_history(pubkey)
    if "error" not in tx_history:
        tx_msgs = []
        for tx in tx_history:
            msg = ""
            status = tx["status"]
            amount = float(tx["amount"])
            icon = "🟢" if amount > 0 else "🔴"
            conj = "⤵️" if amount > 0 else "⤴️"
            date_msg = datetime.datetime.fromtimestamp(tx["date"]).strftime(
                "%m/%d/%Y, %H:%M"
            )
            comment = tx["comment"].replace("<", "&lt;").replace(">", "&gt;")
            msg = f'\n{icon}<code>{tx["pubkey"]}</code>\n{conj} {date_msg} : <b>{tx["amount"]} Ğ1</b>: <i>{comment}</i>'
            if status in ["SENDING", "RECEIVING"]:
                msg += " "+ _(chat_id, context, "PENDING_TX")

            tx_msgs.append(msg)
        if not tx_msgs:
            tx_msgs = [_(chat_id, context, "EMPTY_HISTORIAL")]
        await context.bot.send_message(
            chat_id, text="\n".join(tx_msgs), parse_mode=ParseMode.HTML
        )


async def get_bma_balance(context, chat_id, pubkey):
    # command = f"jaklis idBalance -p {pubkey}"
    print("Checking BMA balance for Pubkey", pubkey, "using endpoint", settings.BMA_ENDPOINT)
    
    try:
        balance = await bma.get_balance(settings.BMA_ENDPOINT, pubkey)
        has_error = False
    except Exception as e:
        traceback.print_exc()  # print the traceback
        msg = _(chat_id, context, "BALANCE_ERROR").format(pubkey=pubkey)
        has_error = True

    if not has_error:
        msg = _(chat_id, context, "BALANCE_RESULT").format(balance=balance)

    await context.bot.send_message(chat_id, text=msg, parse_mode=ParseMode.HTML)

    # await query.edit_message_text(text=msg, parse_mode=ParseMode.HTML)

    return has_error


async def notify_tx(context, tx, chat_id):
    pubkey_to = tx["pubkey"]
    hash = tx["hash"][:5]
    amount = tx["amount"]
    status = (
        tx["status"].replace("RECEIVING", _(chat_id, context, "RECEIVING")).replace("SENDING", _(chat_id, context, "SENDING"))
    )
    status = status.replace("RECEIVED", _(chat_id, context, "RECEIVED")).replace("SENT", _(chat_id, context, "SENT"))
    tx_to_or_from = _(chat_id, context, "TX_FROM") if amount > 0 else _(chat_id, context, "TX_TO")
    comment = tx["comment"]
    comment = _(chat_id, context, "WITH_COMMENT").format(comment=comment) if comment else ""
    msg = f"<b>{status}</b> {tx_to_or_from} <code>{pubkey_to}</code>"
    msg += "\n"+ _(chat_id, context, "AMOUNT").format(amount=amount)
    msg += "\n{comment}"
    await context.bot.send_message(
        chat_id=chat_id, text=msg, parse_mode=ParseMode.HTML
    )


def clean_comment(comment):
    clean_comment = unidecode.unidecode(comment)
    clean_comment = remove_terminal_injections(clean_comment)
    return (
        clean_comment.replace(",", " ")
        .replace("'", " ")
        .replace("`", " ")
        .replace('"', " ")
    )


def payment(dunikey, pubkey, amount, comment, pool=False):
    if comment:
        # comment = clean_comment(comment)
        command = f"jaklis -n '{settings.GVA_NODE}' -k {dunikey} pay -p {pubkey} -a {amount} -c '{comment}'"
    else:
        command = f"jaklis -n '{settings.GVA_NODE}' -k {dunikey} pay -p {pubkey} -a {amount}"
    if pool:
        command += " -m"

    print("command:", command)
    response_cmd = subprocess.getoutput(command)

    print("Response node:", response_cmd)
    if "succès" in response_cmd:
        return TX_SUCCESS
    elif "insufficient balance" in response_cmd:
        return TX_NO_BALANCE
    else:
        return TX_ERROR


def run_json_command(command):
    print("command:", command)
    response_json = subprocess.getoutput(command)
    try:
        response = json.loads(response_json)
    except Exception as error:
        print("response_json", response_json)
        print(traceback.format_exc())
        response = {"error": True}

    return response


def get_user_dunikey(id):
    pubkey = ""
    dunikey_file = join(settings.KEYS_PATH, id + ".dunikey")
    if not os.path.isfile(dunikey_file):
        salt = id
        password = settings.SECRET_KEY
        k = SigningKey.from_credentials(salt, password)
        pubkey = k.pubkey
        k.save_pubsec_file(dunikey_file)
    else:
        pubkey = SigningKey.from_pubsec_file(dunikey_file).pubkey
    return pubkey, dunikey_file


def get_all_active_pubkeys():
    pubkeys = {}
    KEYS_PATH = settings.KEYS_PATH
    onlyfiles = [
        f
        for f in listdir(KEYS_PATH)
        if isfile(join(KEYS_PATH, f))
    ]
    for dunikeyfile in onlyfiles:
        print(dunikeyfile)
        chat_id = os.path.splitext(dunikeyfile)[0]
        full_path_dunikey = join(KEYS_PATH, dunikeyfile)
        pubkey = SigningKey.from_pubsec_file(full_path_dunikey).pubkey
        pubkeys[pubkey] = chat_id
    return pubkeys




async def check_new_tx(pubkey, context: CallbackContext):
    print('check_new_tx', pubkey)
    MAX_MINUTES_LOST_TX = 20
    last_timestamp_notification = int(datetime.datetime.utcnow().timestamp())
    pending_tx = defaultdict(list)
    validated_tx = defaultdict(list)
    lost_tx = {}
    first_run = True
    seconds_passed = 0
    print("2 seconds have passed")
    msg = ""

    if seconds_passed < 60*25:
    # print('last_time', last_timestamp_notification )
        tx_history = get_tx_history(pubkey)
        pprint(tx_history)
        if "error" in tx_history:
            print("Error en la respuesta del history en check_new_tx")
        else:
            pending_hashes = []
            validated_hashes = []

            for tx in tx_history:
                hash = tx["hash"]
                status = tx["status"]

                if status in ["RECEIVED", "SENT"]:
                    validated_hashes.append(hash)
                    if not hash in validated_tx[pubkey]:
                        print(tx)
                        validated_tx[pubkey].append(hash)
                        if not first_run or tx["date"] > last_timestamp_notification:
                            return True
                        if hash in pending_tx[pubkey]:
                            pending_tx[pubkey].remove(hash)

                if status in ["RECEIVING", "SENDING"]:
                    pending_hashes.append(hash)
                    print(tx)
                    if not hash in pending_tx[pubkey]:
                        pending_tx[pubkey].append(hash)
                        #await notify_tx(context, tx, chat_id)

            for hash in pending_tx[pubkey]:
                #                print('Checking pending tx if lost: ', hash)
                if hash not in pending_hashes and hash not in validated_hashes:

                    if hash in lost_tx:
                        time_lost = lost_tx[hash]
                        now = datetime.datetime.utcnow()
                        minutes_diff = (now - time_lost).total_seconds() / 60.0
                        if minutes_diff > MAX_MINUTES_LOST_TX:
                            pending_tx[pubkey].remove(hash)
                            lost_tx.pop(hash)
                            msg = f"Transferencia con firma: <code>#{hash[:5]}</code> no se ha ejecutado.\n<i>Los nodos le perdieron la pista hace más de {MAX_MINUTES_LOST_TX} minutos.</i>"
                            msg += "\nIntentala enviar de nuevo."
                            print(msg)
                            return False
                    else:
                        lost_tx[hash] = datetime.datetime.utcnow()
            first_run = False

        # To detect future changes and send only new notifications from now
        last_timestamp_notification = int(datetime.datetime.utcnow().timestamp())
        time.sleep(60)
        seconds_passed += 60