import asyncio
from telegram import ReplyKeyboardMarkup, __version__ as TG_VER

try:
    from telegram import __version_info__
except ImportError:
    __version_info__ = (0, 0, 0, 0, 0)  # type: ignore[assignment]

if __version_info__ < (20, 0, 0, "alpha", 1):
    raise RuntimeError(
        f"This example is not compatible with your current PTB version {TG_VER}. To view the "
        f"{TG_VER} version of this example, "
        f"visit https://docs.python-telegram-bot.org/en/v{TG_VER}/examples.html"
    )
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update, ForceReply
from telegram.ext import (
    Application,
    CallbackQueryHandler,
    CommandHandler,
    ContextTypes,
    ConversationHandler,
    MessageHandler,
    filters,
    JobQueue,
)
from django.conf import settings
from core.bot_telegram import buttons, scheduled
from core.bot_telegram import (
    TYPING_AMOUNT,
    WAITING_MAIN_BUTTON,
    TYPING_PUBKEY,
    CHOOSING,
    TYPING_AMOUNT,
    TYPING_DESCRIPTION,
    TYPING_CESIUM_NAME,
    TYPING_SEARCHTERM,
    CHOOSING_FROM_SEARCH,
)

application = None

from django.core.management.base import CommandError, BaseCommand


def start_bot() -> None:
    """Run the bot."""
    # Create a new event loop
    # loop = asyncio.new_event_loop()
    # asyncio.set_event_loop(loop)

    global application
    application = Application.builder().token(settings.TELEGRAM_API_KEY).build()
    j = application.job_queue
    j.run_repeating(callback=scheduled.check_gva_servers, interval=10 * 60, first=1)

    j.run_repeating(callback=scheduled.save_stats, interval=24 * 7 * 3600, first=10)
    j.run_repeating(
        callback=scheduled.send_stats, interval=(24 * 7 * 3600) + 50, first=50
    )

    # j.run_repeating(callback=scheduled.check_new_tx, interval=60, first=1)
    conv_handler = ConversationHandler(
        per_message=False,
        # entry_points=[],
        entry_points=[
            MessageHandler(
                filters.TEXT & ~(filters.COMMAND | filters.Regex("^Cancel$")),
                buttons.read_pubkey,
            ),
            CommandHandler("start", buttons.start),
        ],
        states={
            WAITING_MAIN_BUTTON: [
                MessageHandler(
                    filters.TEXT & ~filters.COMMAND | filters.PHOTO, buttons.read_pubkey
                ),
                CallbackQueryHandler(buttons.read_pubkey),
            ],
            TYPING_PUBKEY: [
                MessageHandler(
                    filters.TEXT & ~filters.COMMAND | filters.PHOTO, buttons.read_pubkey
                ),
                CallbackQueryHandler(buttons.read_pubkey),
            ],
            TYPING_CESIUM_NAME: [
                MessageHandler(
                    filters.TEXT & ~filters.COMMAND, buttons.read_cesium_name
                ),
                CallbackQueryHandler(buttons.read_cesium_name),
            ],
            CHOOSING: [
                CallbackQueryHandler(buttons.action_chosen),
                MessageHandler(filters.TEXT & ~filters.COMMAND, buttons.read_pubkey),
            ],
            TYPING_AMOUNT: [
                MessageHandler(
                    filters.TEXT & ~(filters.COMMAND | filters.Regex("^Cancel$")),
                    buttons.read_amount,
                ),
                CallbackQueryHandler(buttons.read_amount),
            ],
            TYPING_DESCRIPTION: [
                MessageHandler(
                    filters.TEXT & ~(filters.COMMAND | filters.Regex("^Cancel$")),
                    buttons.read_payment_comment,
                ),
                CallbackQueryHandler(buttons.read_payment_comment),
            ],
            TYPING_SEARCHTERM: [
                MessageHandler(
                    filters.TEXT & ~(filters.COMMAND | filters.Regex("^Cancel$")),
                    buttons.read_pubkey,
                ),
                CallbackQueryHandler(buttons.read_pubkey),
            ],
            CHOOSING_FROM_SEARCH: [
                CallbackQueryHandler(buttons.searchitem_chosen),
                MessageHandler(filters.TEXT & ~filters.COMMAND, buttons.read_pubkey),
            ],
        },
        fallbacks=[
            MessageHandler(
                filters.TEXT & ~filters.COMMAND | filters.PHOTO, buttons.read_pubkey
            ),
            CallbackQueryHandler(buttons.read_pubkey),
            CommandHandler("start", buttons.start),
        ],
    )

    application.add_handler(conv_handler)
    # application.add_error_handler(error_handler)
    # import asyncio
    # asyncio.run(application.run_polling())

    # loop.create_task(application.run_polling())
    # loop.run_forever()
    application.run_polling()


class Command(BaseCommand):
    help = "Starts the Telegram bot"

    def handle(self, *args, **options):
        start_bot()
