import requests
from django.conf import settings
import json
from requests.exceptions import RequestException
from httpcore import ConnectTimeout

headers = {"content-type": "application/json"}


def check_gva_servers():
    status = {}
    highest_block_number = 0
    for server in settings.GVA_SERVERS:
        url = f"https://{server}/blockchain/current"
        print("Checking", url)
        try:
            response = requests.get(url, headers=headers)
            status[url] = {"ping": response.elapsed.total_seconds()}
            if response.status_code != 200:
                print(f"Error: {response.status_code} - {response.reason}")
                status[url]["ping"] = -1
            else:
                data = json.loads(response.text)
                if "number" in data:
                    blocknumber = data["number"]
                    status[url]["block"] = blocknumber
                    print("Blocknumber:", blocknumber)
                    if blocknumber >= highest_block_number:
                        highest_block_number = blocknumber

        except (ConnectTimeout, RequestException) as e:
            print(f"Exception while connecting to {url}: {e}")
            status[url] = {"ping": -1}

    print("status", status)
    for server, value in status.items():
        if value["ping"] != -1 and value["block"] < highest_block_number - 2:
            # it's at least two blocks unsynced
            value["unsynced"] = True

    return status
